package ru.com.pershinanton.medsolution.main.base;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.hwangjr.rxbus.RxBus;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseMvpPresenter<T extends MvpView> extends MvpPresenter<T> {

    private final boolean useBus;

    public BaseMvpPresenter(boolean useBus) {
        this.useBus = useBus;
        if (useBus)
            RxBus.get().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(useBus)
            RxBus.get().unregister(this);

    }

}