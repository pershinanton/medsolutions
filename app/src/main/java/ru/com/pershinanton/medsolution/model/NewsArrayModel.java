package ru.com.pershinanton.medsolution.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ru.com.pershinanton.medsolution.db.persister.GsonNewsCategoryModelPersister;
import ru.com.pershinanton.medsolution.db.persister.GsonStaticticsModelPersister;

@DatabaseTable(tableName = "NEWS")
public class NewsArrayModel {
    private static final String ID = "id";
    private static final String LINK = "link";
    private static final String TITLE = "title";
    private static final String STATISTICS = "statistics";
    private static final String CREATED_AT = "created_at";
    private static final String IMAGE_URL = "image_url";
    private static final String TEXT = "text";
    private static final String NEWS_CATEGORY = "news_category";

    @SerializedName(ID)
    @DatabaseField(columnName = ID, id = true)
    private long id;
    @SerializedName(LINK)
    @DatabaseField(columnName = LINK)
    private String url;
    @SerializedName(TITLE)
    @DatabaseField(columnName = TITLE)
    private String title;
    @SerializedName(STATISTICS)
    @DatabaseField(columnName = STATISTICS, persisterClass = GsonStaticticsModelPersister.class)
    private StaticticsModel staticticsModel;
    @SerializedName(CREATED_AT)
    @DatabaseField(columnName = CREATED_AT)
    private String date;
    @SerializedName(IMAGE_URL)
    @DatabaseField(columnName = IMAGE_URL)
    private String imageUrl;
    @SerializedName(TEXT)
    @DatabaseField(columnName = TEXT)
    private String text;
    @SerializedName(NEWS_CATEGORY)
    @DatabaseField(columnName = NEWS_CATEGORY, persisterClass = GsonNewsCategoryModelPersister.class)
    private NewsCategoryModel newsCategoryModel;

    public NewsArrayModel() {
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public StaticticsModel getStaticticsModel() {
        return staticticsModel;
    }

    public String getDate() {
        return date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public NewsCategoryModel getNewsCategoryModel() {
        return newsCategoryModel;
    }

}
