package ru.com.pershinanton.medsolution.retrofit.exception;

public class InvalidUserIdException extends Exception {


    public InvalidUserIdException() {
        super();
    }

    public InvalidUserIdException(String massage) {
        super(massage);
    }
}
