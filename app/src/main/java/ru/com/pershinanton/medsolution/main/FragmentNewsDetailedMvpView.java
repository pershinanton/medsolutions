package ru.com.pershinanton.medsolution.main;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.com.pershinanton.medsolution.model.NewsDetailedModel;

public interface FragmentNewsDetailedMvpView extends MvpView {
    @StateStrategyType(AddToEndStrategy.class)
    void onDataLoaded(NewsDetailedModel newsDetailedModel);
    @StateStrategyType(AddToEndStrategy.class)
    void onLoading(boolean isLoading);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onDataError(String message);


}
