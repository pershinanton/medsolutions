package ru.com.pershinanton.medsolution.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {// <T> указывается какой будет тип объекта response
    @SerializedName("error")
    public ErrorResponse error;
    @SerializedName("access_token")
    public String accessToken;
    @SerializedName("token_type")
    public String tokenType;
    @SerializedName("refresh_token")
    public String refreshToken;

    public ErrorResponse getError() {
        return error;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}