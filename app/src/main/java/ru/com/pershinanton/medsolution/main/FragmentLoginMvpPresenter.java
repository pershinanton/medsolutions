package ru.com.pershinanton.medsolution.main;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.main.base.BaseMvpPresenter;
import ru.com.pershinanton.medsolution.preferences.AuthPreferences;
import ru.com.pershinanton.medsolution.retrofit.ApiClient;
import ru.com.pershinanton.medsolution.retrofit.exception.UnknownApiException;
import ru.com.pershinanton.medsolution.retrofit.model.LoginResponse;
import ru.com.pershinanton.medsolution.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class FragmentLoginMvpPresenter extends BaseMvpPresenter<FragmentLoginMvpView> implements BackButtonListener {

    @Inject
    ApiClient apiClient;
    @Inject
    AuthPreferences authPreferences;
    private Router router;
    private Disposable disposable;

    public FragmentLoginMvpPresenter(Router router) {
        super(false);
        this.router = router;
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void login(String email, String password) {
        dispose();
        disposable = Flowable.just(true)
                .subscribeOn(Schedulers.newThread())
                .map((Boolean value) -> {
                    Response<LoginResponse> response;
                    try {
                        response = apiClient.loginPost("android", "application/json", "password", email, password).execute();
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }
                    if (response == null) throw new UnknownApiException();
                    if (response.code() != 200) throw new UnknownApiException();
                    if (response.errorBody() != null) throw new UnknownApiException();
                    if (response.body() == null) throw new UnknownApiException();
                    LoginResponse result = response.body();

                    return result;

                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    getViewState().onDataLoaded(result);
                }, throwable -> {
                    onThrowable(throwable);
                });
    }

    protected void dispose() {
        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }
            disposable = null;
        }
    }

    private void onThrowable(Throwable throwable) {
        getViewState().onDataError(throwable.getMessage());
    }

    @Override
    public void onDestroy() {
        dispose();
        super.onDestroy();
        router = null;
    }


    @Override
    public boolean onBackPressed() {
        if (router != null) router.exit();
        return true;
    }

}