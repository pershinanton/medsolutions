package ru.com.pershinanton.medsolution.model;

import com.google.gson.annotations.SerializedName;

public class OneNewsModel {
    @SerializedName("news")
    private NewsDetailedModel newsDetailedModel;

    public NewsDetailedModel getNewsDetailedModel() {
        return newsDetailedModel;
    }
}