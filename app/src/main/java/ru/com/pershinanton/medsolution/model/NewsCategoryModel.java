package ru.com.pershinanton.medsolution.model;

import com.google.gson.annotations.SerializedName;

public class NewsCategoryModel {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("slug")
    private String slug;

    public int getIds() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSlug() {
        return slug;
    }
}