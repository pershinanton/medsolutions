package ru.com.pershinanton.medsolution.network;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.com.pershinanton.medsolution.LogoutController;
import ru.com.pershinanton.medsolution.preferences.AuthPreferences;
import ru.com.pershinanton.medsolution.retrofit.exception.UnauthorizedIOException;

@Singleton
public class AuthenticationInterceptor implements Interceptor {

    private final AuthPreferences authPreferences;
    private final LogoutController logoutController;

    @Inject
    public AuthenticationInterceptor(AuthPreferences authPreferences, LogoutController logoutController) {
        this.authPreferences = authPreferences;
        this.logoutController = logoutController;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        if (authPreferences.isSigned())
            builder.header("Authorization", authPreferences.getTokenType() + " " + authPreferences.getAccessToken());
        builder.header("Client-Type", "android");

        Request request = builder.build();
        Response response = chain.proceed(request);
        if (response.code() == 401) {
            logoutController.unauthorized();
            throw new UnauthorizedIOException();
        }
        return response;
    }

}