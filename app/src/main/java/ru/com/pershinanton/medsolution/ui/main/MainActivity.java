package ru.com.pershinanton.medsolution.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;

import javax.inject.Inject;

import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.BusAction;
import ru.com.pershinanton.medsolution.R;
import ru.com.pershinanton.medsolution.Screens;
import ru.com.pershinanton.medsolution.dagger.Toaster;
import ru.com.pershinanton.medsolution.preferences.AuthPreferences;
import ru.com.pershinanton.medsolution.ui.base.BaseActivity;
import ru.com.pershinanton.medsolution.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import ru.terrakok.cicerone.commands.BackTo;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;

public class MainActivity extends BaseActivity {

    @Inject
    Toaster toaster;
    @Inject
    AuthPreferences authPreferences;
    @Inject
    NavigatorHolder navigatorHolder;

    @Subscribe(
            thread = EventThread.MAIN_THREAD,
            tags = {
                    @Tag(BusAction.AUTH_STATE_UNAUTHORIZED)
            }
    )
    public void authStateUnauthorized(Boolean authorized) {
        if (navigator != null)
            navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_LOGIN, null)});
    }

    private final Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.container) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case Screens.FRAGMENT_LOGIN:
                    return FragmentLogin.getNewInstance(/*(FragmentNews.InstantiateObject) data*/);
                case Screens.FRAGMENT_WALL:
                    return FragmentNews.getNewInstance(/*(FragmentNews.InstantiateObject) data*/);
                case Screens.FRAGMENT_NEWS_VIEW:
                    return FragmentNewsView.getNewInstance((FragmentNewsView.InstantiateObject) data);

            }
            return null;
        }

        @Override
        protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
            fragmentTransaction.setCustomAnimations(
                    R.animator.fade_in,
                    R.animator.fade_out,
                    R.animator.fade_in,
                    R.animator.fade_out);
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }

        @Override
        public void applyCommands(Command[] commands) {
            try {
                super.applyCommands(commands);
                getSupportFragmentManager().executePendingTransactions();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        initRxBus();
    }

    @Override
    public int onInflateLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onViewInflated(Bundle savedInstanceState) {
        initButterKnife();
        if (savedInstanceState == null)
            if (authPreferences.isSigned()) {
                navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_WALL, null/* new FragmentNews.InstantiateObject(MovieList.getList())*/)});
            } else {
                navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_LOGIN, null/* new FragmentNews.InstantiateObject(MovieList.getList())*/)});
            }
    }

    @Override
    protected void onResumeFragments() {
        if (navigatorHolder != null) {
            super.onResumeFragments();
            navigatorHolder.setNavigator(navigator);
        }
    }

    @Override
    protected void onPause() {
        if (navigatorHolder != null) navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null
                && fragment instanceof BackButtonListener
                && ((BackButtonListener) fragment).onBackPressed()) {
            return;
        } else {
            super.onBackPressed();
        }
    }

}