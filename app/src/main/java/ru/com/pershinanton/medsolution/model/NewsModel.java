package ru.com.pershinanton.medsolution.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsModel {
    @SerializedName("news")
    private List<NewsArrayModel> newsArrayModels;

    public List<NewsArrayModel> getNewsArrayModels() {
        return newsArrayModels;
    }
}