package ru.com.pershinanton.medsolution.main;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.LogoutController;
import ru.com.pershinanton.medsolution.db.helper.DatabaseHelper;
import ru.com.pershinanton.medsolution.main.base.BaseNetworkMvpPresenter;
import ru.com.pershinanton.medsolution.model.NewsArrayModel;
import ru.com.pershinanton.medsolution.preferences.AuthPreferences;
import ru.com.pershinanton.medsolution.retrofit.ApiClient;
import ru.com.pershinanton.medsolution.retrofit.exception.UnknownApiException;
import ru.com.pershinanton.medsolution.retrofit.model.WallResponse;
import ru.com.pershinanton.medsolution.rxjava.RetryWithDelay;
import ru.com.pershinanton.medsolution.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class FragmentNewsMvpPresenter extends BaseNetworkMvpPresenter<FragmentNewsMvpView> implements BackButtonListener {// в этом классе мы получаем данные из других мест

    @Inject
    ApiClient apiClient;
    @Inject
    DatabaseHelper databaseHelper;
    @Inject
    AuthPreferences authPreferences;
    @Inject
    LogoutController logoutController;
    private Router router;
    private Disposable disposable;

    public FragmentNewsMvpPresenter(Router router) {
        super(false);
        this.router = router;
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadFromCache();

    }

    public void loadFromCache() {
        getViewState().onLoading(true);
        disposable = Flowable.just(true)
                .subscribeOn(Schedulers.newThread())
                .retry(throwable -> onRequestRetryNetwork(throwable))
                .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
                .map((Boolean value) -> databaseHelper.getAllNewsArrayModel()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    if(items != null) {
                        getViewState().onDataLoaded(items, true, 0);
                        getViewState().onLoading(false);
                    }
                    load(0);
                }, throwable -> {
                    onThrowable(throwable);
                    getViewState().onLoading(false);
                });
    }

    public void reload() {
        getViewState().onLoading(true);
        load(0);
    }

    public void load(int offset) {
        dispose();
        disposable = Flowable.just(true)
                .subscribeOn(Schedulers.newThread())
                .retry(throwable -> onRequestRetryNetwork(throwable))
                .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
                .map((Boolean value) -> {
                    Response<WallResponse> response;
                    try {                                           // response ответ с сервера по обращению
                        response = apiClient.wallGet((offset / ApiClient.ITEMS_PER_PAGE) + 1, ApiClient.ITEMS_PER_PAGE).execute();
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }
                    if (response == null) throw new UnknownApiException();
                    if (response.code() != 200) throw new UnknownApiException();
                    if (response.errorBody() != null) throw new UnknownApiException();
                    if (response.body() == null) throw new UnknownApiException();
                    WallResponse apiResponse = response.body();

                    List<NewsArrayModel> items = apiResponse.getNewsModel().getNewsArrayModels();
                    if (items == null) throw new UnknownApiException();

                    if (offset == 0) {
                        databaseHelper.clearAllNewsArrayModel();
                        for (NewsArrayModel model : items)
                            databaseHelper.createOrUpdateNewsArrayModel(model);
                    }

                    return items;
//
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    getViewState().onDataLoaded(items, false, offset);
                    getViewState().onLoading(false);
                }, throwable -> {
                    onThrowable(throwable);
                    getViewState().onLoading(false);
                });
    }

    //
    protected void dispose() {
        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }
            disposable = null;
        }
    }

    @Override
    public void onDestroy() {
        dispose();
        super.onDestroy();
        router = null;
    }


    @Override
    public boolean onBackPressed() {
        if (router != null) router.exit();
        return true;
    }

}