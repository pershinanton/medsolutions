package ru.com.pershinanton.medsolution.retrofit.model;

import com.google.gson.annotations.SerializedName;

import ru.com.pershinanton.medsolution.model.OneNewsModel;

public class NewsResponse<N> {// <T> указывается какой будет тип объекта response
    @SerializedName("success")
    public String success;
    @SerializedName("data")
    public OneNewsModel oneNewsModel;
    @SerializedName("error")
    public ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public String getSuccess() {
        return success;
    }

    public OneNewsModel getOneNewsModel() {
        return oneNewsModel;
    }
}