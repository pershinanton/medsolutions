package ru.com.pershinanton.medsolution.recycleview.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ru.com.pershinanton.medsolution.R;

public class WallViewHolder extends RecyclerView.ViewHolder {
    public final TextView category;
    public final TextView title;
    public final TextView date;
    public final ImageView url;

    public WallViewHolder(View view) {
        super(view);
        title = view.findViewById(R.id.title);
        category = view.findViewById(R.id.category);
        url = view.findViewById(R.id.url);
        date = view.findViewById(R.id.date);

    }

}