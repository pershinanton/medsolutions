package ru.com.pershinanton.medsolution.ui.common;

public interface BackButtonListener {
    boolean onBackPressed();
}
