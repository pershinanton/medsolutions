package ru.com.pershinanton.medsolution.dagger;

import android.app.Application;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Toaster {

    private final Application application;

    @Inject
    public Toaster(Application application) {
        this.application = application;
    }

    public void toastShort(String text) {
        Toast.makeText(application, text, Toast.LENGTH_SHORT).show();

    }

    public void toastLong(String text) {
        Toast.makeText(application, text, Toast.LENGTH_LONG).show();

    }

}
