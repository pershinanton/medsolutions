package ru.com.pershinanton.medsolution.retrofit.model;

import com.google.gson.annotations.SerializedName;

import ru.com.pershinanton.medsolution.model.NewsModel;

public class WallResponse<N> {// <T> указывается какой будет тип объекта response
    @SerializedName("success")
    public String success;
    @SerializedName("data")
    public NewsModel newsModel;
    @SerializedName("error")
    public ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public String getSuccess() {
        return success;
    }

    public NewsModel getNewsModel() {
        return newsModel;
    }
}