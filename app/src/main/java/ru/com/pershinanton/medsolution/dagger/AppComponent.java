package ru.com.pershinanton.medsolution.dagger;


import javax.inject.Singleton;

import dagger.Component;
import ru.com.pershinanton.medsolution.dagger.module.ApplicationModule;
import ru.com.pershinanton.medsolution.dagger.module.JsonModule;
import ru.com.pershinanton.medsolution.dagger.module.NavigationModule;
import ru.com.pershinanton.medsolution.dagger.module.NetworkModule;
import ru.com.pershinanton.medsolution.db.persister.GsonCollectionPersister;
import ru.com.pershinanton.medsolution.db.persister.GsonHashMapPersister;
import ru.com.pershinanton.medsolution.db.persister.GsonNewsCategoryModelPersister;
import ru.com.pershinanton.medsolution.db.persister.GsonObjectPersister;
import ru.com.pershinanton.medsolution.db.persister.GsonStaticticsModelPersister;
import ru.com.pershinanton.medsolution.lyra.GsonCollectionStateCoder;
import ru.com.pershinanton.medsolution.lyra.GsonHashMapStateCoder;
import ru.com.pershinanton.medsolution.lyra.GsonObjectStateCoder;
import ru.com.pershinanton.medsolution.main.FragmentLoginMvpPresenter;
import ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpPresenter;
import ru.com.pershinanton.medsolution.main.FragmentNewsMvpPresenter;
import ru.com.pershinanton.medsolution.ui.main.FragmentLogin;
import ru.com.pershinanton.medsolution.ui.main.FragmentNews;
import ru.com.pershinanton.medsolution.ui.main.FragmentNewsView;
import ru.com.pershinanton.medsolution.ui.main.MainActivity;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NavigationModule.class,
        NetworkModule.class,
        JsonModule.class
})
public interface AppComponent {
    void inject(MainActivity activity);

    void inject(GsonObjectStateCoder gsonObjectStateCoder);

    void inject(GsonHashMapStateCoder gsonHashMapStateCoder);

    void inject(GsonCollectionStateCoder gsonCollectionStateCoder);

    void inject(FragmentLogin fragmentLogin);

    void inject(FragmentNews fragmentNews);

    void inject(FragmentNewsView fragmentNewsView);

    void inject(FragmentLoginMvpPresenter fragmentLoginMvpPresenter);

    void inject(FragmentNewsMvpPresenter fragmentNewsMvpPresenter);

    void inject(FragmentNewsDetailedMvpPresenter fragmentNewsDetailedMvpPresenter);

    void inject(GsonObjectPersister gsonObjectPersister);

    void inject(GsonHashMapPersister gsonHashMapPersister);

    void inject(GsonCollectionPersister gsonCollectionPersister);

    void inject(GsonStaticticsModelPersister gsonStaticticsModelPersister);

    void inject(GsonNewsCategoryModelPersister gsonNewsCategoryModelPersister);

}


