package ru.com.pershinanton.medsolution.dagger.module;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.com.pershinanton.medsolution.gson.DeserializationExclusionStrategy;
import ru.com.pershinanton.medsolution.gson.SerializationExclusionStrategy;

@Module
@Singleton
public class JsonModule {
    @Provides
    @Singleton
    Gson gson() {
        return new GsonBuilder()
//                                        .registerTypeAdapter(NewsPartModel.class, new CartoonDetailedContentModelDeserializer())
                .addDeserializationExclusionStrategy(new DeserializationExclusionStrategy())
                .addSerializationExclusionStrategy(new SerializationExclusionStrategy())
                .create();
    }

}