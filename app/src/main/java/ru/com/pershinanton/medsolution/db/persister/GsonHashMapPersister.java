package ru.com.pershinanton.medsolution.db.persister;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.StringType;

import javax.inject.Inject;

import ru.com.pershinanton.medsolution.App;

public class GsonHashMapPersister extends StringType {

    private static final GsonHashMapPersister INSTANCE = new GsonHashMapPersister();
    @Inject
    Gson gson;

    public GsonHashMapPersister() {
        super(SqlType.STRING, new Class<?>[]{new TypeToken<Object>() {
        }.getClass()});
        App.INSTANCE.getAppComponent().inject(this);
    }

    public static GsonHashMapPersister getSingleton() {
        return INSTANCE;
    }

    @Override
    public String javaToSqlArg(FieldType fieldType, Object object) {
        if (object != null)
            return gson.toJson(object, new TypeToken<Object>() {
            }.getType());
        return null;
    }

    @Override
    public Object sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        Object object = null;
        if (sqlArg != null)
            object = gson.fromJson((String) sqlArg, new TypeToken<Object>() {
            }.getType());
        return object;
    }

}