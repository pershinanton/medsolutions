package ru.com.pershinanton.medsolution.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.R;
import ru.com.pershinanton.medsolution.Screens;
import ru.com.pershinanton.medsolution.main.FragmentNewsMvpPresenter;
import ru.com.pershinanton.medsolution.main.FragmentNewsMvpView;
import ru.com.pershinanton.medsolution.model.NewsArrayModel;
import ru.com.pershinanton.medsolution.recycleview.adapter.NewsPaginationAdapter;
import ru.com.pershinanton.medsolution.recycleview.adapter.utils.RequestLoadNextController;
import ru.com.pershinanton.medsolution.ui.base.BaseFragment;
import ru.com.pershinanton.medsolution.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;

public class FragmentNews extends BaseFragment implements FragmentNewsMvpView, BackButtonListener {

    @Inject
    Router router;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar1)
    Toolbar toolbar;
    @BindView(R.id.progress_wheel)
    ProgressWheel progressWheel;
    @InjectPresenter
    FragmentNewsMvpPresenter presenter;
    MainActivity activity;
    private NewsPaginationAdapter adapter;

    public FragmentNews() {
        super(true, false, Orientation.NONE);
    }

    public static FragmentNews getNewInstance(/*InstantiateObject instantiateObject*/) {
        FragmentNews fragment = new FragmentNews();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @ProvidePresenter
    public FragmentNewsMvpPresenter createPresenter() {
        return new FragmentNewsMvpPresenter(router);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
        progressWheel.setVisibility(View.INVISIBLE);
        toolbar.setTitle("Новости");
        toolbar.inflateMenu(R.menu.news_menu);
        toolbar.setTitleTextColor(ContextCompat.getColor(activity, R.color.white));
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.update) {
                    adapter.clearModels();
                    if (recyclerView != null) recyclerView.scrollToPosition(0);
                    presenter.reload();
                }
                return true;
            }
        });
        setHasOptionsMenu(true);
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.line_divider));
        recyclerView.addItemDecoration(itemDecorator);
        adapter = new NewsPaginationAdapter(activity, new NewsPaginationAdapter.Listener() {
            @Override
            public void onClickWall(NewsArrayModel newsArrayModel) {
                router.navigateTo(Screens.FRAGMENT_NEWS_VIEW, new FragmentNewsView.InstantiateObject(newsArrayModel.getId()));
            }
        }).setRequestLoadNextListener(new RequestLoadNextController.OnRequestLoadNextListener() {
            @Override
            public void onRequestLoad() {
                presenter.load(adapter.getItemCount());

            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public int onInflateLayout() {
        return R.layout.fragment_news;
    }

    @Override
    public boolean onBackPressed() {
        if (presenter != null) return presenter.onBackPressed();
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    @Override
    public void onDataLoaded(List<NewsArrayModel> items, boolean fromCache, int offset) {
        if(fromCache) {
            if (adapter != null) adapter.setModels(items, false);
        } else {
            if (adapter != null)
                if (offset == 0) adapter.setModels(items, true);
                else adapter.addModels(items);
        }
    }

    @Override
    public void onLoading(boolean isLoading) {
        if(isLoading){
            progressWheel.setVisibility(View.VISIBLE);
            progressWheel.spin();
        }else {
            progressWheel.setVisibility(View.INVISIBLE);
            progressWheel.stopSpinning();
        }
    }

    @Override
    public void onDataError(String message) {
        if (message != null)
            router.showSystemMessage(message);
    }

    @Override
    public void connectionException() {
        if (router != null) router.showSystemMessage(getString(R.string.error_connection));
    }

    @Override
    public void unknownException() {
        if (router != null) router.showSystemMessage(getString(R.string.error_unknown));
    }

    @Override
    public void apiException(String message) {
        if (message != null && message.length() > 0) {
            if (router != null) router.showSystemMessage(message);
        } else unknownException();
    }

}
