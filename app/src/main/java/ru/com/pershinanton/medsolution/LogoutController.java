package ru.com.pershinanton.medsolution;

import com.hwangjr.rxbus.RxBus;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.com.pershinanton.medsolution.preferences.AuthPreferences;


@Singleton
public class LogoutController {

    AuthPreferences authPreferences;

    @Inject
    public LogoutController(AuthPreferences authPreferences) {
        this.authPreferences = authPreferences;
    }

    public void unauthorized() {
        authPreferences.clear();
        RxBus.get().post(BusAction.AUTH_STATE_UNAUTHORIZED, true);
    }

}