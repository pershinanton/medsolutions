package ru.com.pershinanton.medsolution.ui.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.fondesa.lyra.annotation.SaveState;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.sufficientlysecure.htmltextview.HtmlTextView;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.R;
import ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpPresenter;
import ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView;
import ru.com.pershinanton.medsolution.model.NewsDetailedModel;
import ru.com.pershinanton.medsolution.ui.base.BaseFragment;
import ru.com.pershinanton.medsolution.utils.DateFormatter;
import ru.com.pershinanton.medsolution.utils.UilHelper;
import ru.terrakok.cicerone.Router;

public class FragmentNewsView extends BaseFragment implements FragmentNewsDetailedMvpView {


    @BindView(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout collapsing_toolbar_layout;
    @BindView(R.id.app_bar_layout)
    AppBarLayout app_bar_layout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.header_image_view)
    ImageView header_image_view;
    @BindView(R.id.category)
    HtmlTextView category;
    @BindView(R.id.textView)
    HtmlTextView textView;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.title)
    HtmlTextView title;
    @BindView(R.id.buttonLink)
    TextView buttonLink;
    @BindView(R.id.progress_wheel)
    ProgressWheel progressWheel;
    @Inject
    Router router;
    @InjectPresenter
    FragmentNewsDetailedMvpPresenter presenter;

    @SaveState
    int textSizePosition = -1;

    MainActivity activity;

    public FragmentNewsView() {
        super(true, false, Orientation.NONE);
    }

    public static FragmentNewsView getNewInstance(InstantiateObject instantiateObject) {
        FragmentNewsView fragment = new FragmentNewsView();
        Bundle bundle = new Bundle();
        bundle.putParcelable(INSTANTIATE_OBJECT, instantiateObject);
        fragment.setArguments(bundle);
        return fragment;
    }

    @ProvidePresenter
    public FragmentNewsDetailedMvpPresenter createPresenter() {
        return new FragmentNewsDetailedMvpPresenter(router, getNewsId());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
        progressWheel.setVisibility(View.INVISIBLE);
        toolbar.inflateMenu(R.menu.news_one_menu);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onBackPressed();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.textSize) {
                    toggleTextSize();
                    return true;
                }
                return false;
            }
        });

        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;

                } else if (isShow) {
                    isShow = false;

                }
            }
        });
        setHasOptionsMenu(true);
        configureTextSize();
    }

    private void toggleTextSize() {
        if(textSizePosition < 2) textSizePosition ++;
        else textSizePosition = 0;
        configureTextSize();
    }

    private void configureTextSize() {
        switch (textSizePosition) {
            case 0:
                category.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
                date.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                break;
            case 1:
                category.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
                title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
                date.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                break;
            case 2:
                category.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                date.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                break;
        }
    }

    @Override
    public int onInflateLayout() {
        return R.layout.fragment_news_view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    @Override
    public void onLoading(boolean isLoading) {
        if (isLoading) {
            progressWheel.setVisibility(View.VISIBLE);
            progressWheel.spin();
        } else {
            progressWheel.setVisibility(View.INVISIBLE);
            progressWheel.stopSpinning();
        }
    }

    @Override
    public void onDataLoaded(NewsDetailedModel newsDetailedModel) {
        ImageLoader.getInstance().displayImage(newsDetailedModel.getImageUrl(), header_image_view, UilHelper.getOptions()
                .resetViewBeforeLoading(true)
                .build());
        category.setHtml(newsDetailedModel.getNewsCategoryModel().getTitle().toUpperCase());
        category.setTypeface(null, Typeface.BOLD);
        title.setHtml(newsDetailedModel.getTitle());
        title.setTypeface(null, Typeface.BOLD);
        date.setText(DateFormatter.formatDateTime(newsDetailedModel.getDate(), true));
        textView.setHtml(newsDetailedModel.getText());
        buttonLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(newsDetailedModel.getUrl()));
                startActivity(browserIntent);
            }
        });
    }

    @Override
    public void onDataError(String message) {
        if (message != null)
            router.showSystemMessage(message);
    }


    private InstantiateObject getIntantiataeObject() {
        return getArguments().getParcelable(INSTANTIATE_OBJECT);
    }

    private long getNewsId() {
        return getIntantiataeObject().getNewsId();
    }

    public static class InstantiateObject implements Parcelable {

        public static final Creator<InstantiateObject> CREATOR = new Creator<InstantiateObject>() {
            @Override
            public InstantiateObject createFromParcel(Parcel source) {
                return new InstantiateObject(source);
            }

            @Override
            public InstantiateObject[] newArray(int size) {
                return new InstantiateObject[size];
            }
        };
        private final long id;

        public InstantiateObject(long id) {
            this.id = id;

        }

        protected InstantiateObject(Parcel in) {
            this.id = in.readLong();

        }

        public long getNewsId() {
            return id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(this.id);
        }
    }

}
