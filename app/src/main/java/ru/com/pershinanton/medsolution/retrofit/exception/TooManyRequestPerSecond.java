package ru.com.pershinanton.medsolution.retrofit.exception;

public class TooManyRequestPerSecond extends Exception {
    public TooManyRequestPerSecond() {
        super();
    }

    public TooManyRequestPerSecond(String message) {
        super(message);
    }
}
