package ru.com.pershinanton.medsolution.db.persister;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.StringType;

import javax.inject.Inject;

import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.model.StaticticsModel;

public class GsonStaticticsModelPersister extends StringType {

    private static final GsonStaticticsModelPersister INSTANCE = new GsonStaticticsModelPersister();
    @Inject
    Gson gson;

    public GsonStaticticsModelPersister() {
        super(SqlType.STRING, new Class<?>[]{new TypeToken<StaticticsModel>() {
        }.getClass()});
        App.INSTANCE.getAppComponent().inject(this);
    }

    public static GsonStaticticsModelPersister getSingleton() {
        return INSTANCE;
    }

    @Override
    public String javaToSqlArg(FieldType fieldType, Object StaticticsModel) {
        if (StaticticsModel != null)
            return gson.toJson(StaticticsModel, new TypeToken<StaticticsModel>() {
            }.getType());
        return null;
    }

    @Override
    public StaticticsModel sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        StaticticsModel StaticticsModel = null;
        if (sqlArg != null)
            StaticticsModel = gson.fromJson((String) sqlArg, new TypeToken<StaticticsModel>() {
            }.getType());
        return StaticticsModel;
    }

}