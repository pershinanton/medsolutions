package ru.com.pershinanton.medsolution.preferences;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AuthPreferences {

    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String TOKEN_TYPE = "TOKEN_TYPE";
    private static final String PREFS_FILENAME = AuthPreferences.class.getSimpleName();

    private final SharedPreferences prefs;
    private String accessToken;
    private String tokenType;

    @Inject
    public AuthPreferences(Application application) {
        prefs = application.getSharedPreferences(PREFS_FILENAME,
                Context.MODE_PRIVATE);
    }

    public String getAccessToken() {
        if (accessToken == null)
            accessToken = prefs.getString(ACCESS_TOKEN, null);
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(ACCESS_TOKEN, accessToken);
        edit.commit();
    }

    public String getTokenType() {
        if (tokenType == null)
            tokenType = prefs.getString(TOKEN_TYPE, null);
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(TOKEN_TYPE, tokenType);
        edit.commit();
    }

    public boolean isSigned() {
        if (getAccessToken() != null && getAccessToken().length() > 0 && getTokenType() != null && getTokenType().length() > 0)
            return true;
        return false;
    }

    public void clear() {
        accessToken = null;
        tokenType = null;
        prefs.edit().clear().commit();
    }
}