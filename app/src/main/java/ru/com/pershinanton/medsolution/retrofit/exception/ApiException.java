package ru.com.pershinanton.medsolution.retrofit.exception;

public class ApiException extends Exception {

    public ApiException() {
        super();
    }

    public ApiException(String message) {
        super(message);
    }

}
