package ru.com.pershinanton.medsolution.main;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.LogoutController;
import ru.com.pershinanton.medsolution.main.base.BaseMvpPresenter;
import ru.com.pershinanton.medsolution.model.NewsDetailedModel;
import ru.com.pershinanton.medsolution.retrofit.ApiClient;
import ru.com.pershinanton.medsolution.retrofit.exception.UnknownApiException;
import ru.com.pershinanton.medsolution.retrofit.model.NewsResponse;
import ru.com.pershinanton.medsolution.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class FragmentNewsDetailedMvpPresenter extends BaseMvpPresenter<FragmentNewsDetailedMvpView> implements BackButtonListener {

    private final long newsId;

    @Inject
    ApiClient apiClient;
    @Inject
    LogoutController logoutController;
    private Router router;
    private Disposable disposable;

    public FragmentNewsDetailedMvpPresenter(Router router, long newsId) {
        super(false);
        this.router = router;
        this.newsId = newsId;
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        load();
    }

    private void load() {
        getViewState().onLoading(true);
        disposable = Flowable.just(true)
                .subscribeOn(Schedulers.newThread())
                .map((Boolean value) -> {
                    Response<NewsResponse> response;
                    try {
                        response = apiClient.newsGet(newsId).execute();
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }

                    if (response == null) throw new UnknownApiException();
                    if (response.code() != 200) throw new UnknownApiException();
                    if (response.errorBody() != null) throw new UnknownApiException();
                    if (response.body() == null) throw new UnknownApiException();
                    NewsResponse apiResponse = response.body();


                    NewsDetailedModel items = apiResponse.getOneNewsModel().getNewsDetailedModel();
                    if (items == null) throw new UnknownApiException();

                    return items;
//
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    getViewState().onDataLoaded(items);
                    getViewState().onLoading(false);
                }, throwable -> {
                    onThrowable(throwable);
                    getViewState().onLoading(false);
                });
    }

    protected void dispose() {
        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }
            disposable = null;
        }
    }

    private void onThrowable(Throwable throwable) {
        getViewState().onDataError(throwable.getMessage());
    }

    @Override
    public void onDestroy() {
        dispose();
        super.onDestroy();
        router = null;
    }


    @Override
    public boolean onBackPressed() {
        if (router != null) router.exit();
        return true;
    }

}