package ru.com.pershinanton.medsolution.model;

import com.google.gson.annotations.SerializedName;

public class StaticticsModel {

    @SerializedName("show")
    private String show;
    @SerializedName("time")
    private String time;
    @SerializedName("share")
    private String share;
    @SerializedName("source")
    private String source;
    @SerializedName("perusal")
    private String perusal;

    public String getShow() {
        return show;
    }

    public String getTime() {
        return time;
    }

    public String getShare() {
        return share;
    }

    public String getSource() {
        return source;
    }

    public String getPerusal() {
        return perusal;
    }

}