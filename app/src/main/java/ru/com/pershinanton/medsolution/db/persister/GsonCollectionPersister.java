package ru.com.pershinanton.medsolution.db.persister;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.StringType;

import java.util.Collection;

import javax.inject.Inject;

import ru.com.pershinanton.medsolution.App;

public class GsonCollectionPersister extends StringType {

    private static final GsonCollectionPersister INSTANCE = new GsonCollectionPersister();
    @Inject
    Gson gson;

    public GsonCollectionPersister() {
        super(SqlType.STRING, new Class<?>[]{new TypeToken<Collection<Object>>() {
        }.getClass()});
        App.INSTANCE.getAppComponent().inject(this);
    }

    public static GsonCollectionPersister getSingleton() {
        return INSTANCE;
    }

    @Override
    public String javaToSqlArg(FieldType fieldType, Object object) {
        if (object != null)
            return gson.toJson(object, new TypeToken<Collection<Object>>() {
            }.getType());
        return null;
    }

    @Override
    public Collection<Object> sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        Collection<Object> object = null;
        if (sqlArg != null)
            object = gson.fromJson((String) sqlArg, new TypeToken<Collection<Object>>() {
            }.getType());
        return object;
    }

}