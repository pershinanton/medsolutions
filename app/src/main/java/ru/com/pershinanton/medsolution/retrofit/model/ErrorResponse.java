package ru.com.pershinanton.medsolution.retrofit.model;

import com.google.gson.annotations.SerializedName;

public class ErrorResponse {

    @SerializedName("error")
    public int error;
    @SerializedName("error_description")
    public String errorDescription;

    public int getError() {
        return error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}