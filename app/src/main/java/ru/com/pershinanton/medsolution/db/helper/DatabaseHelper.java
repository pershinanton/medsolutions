package ru.com.pershinanton.medsolution.db.helper;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DataPersisterManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.com.pershinanton.medsolution.db.persister.GsonNewsCategoryModelPersister;
import ru.com.pershinanton.medsolution.db.persister.GsonStaticticsModelPersister;
import ru.com.pershinanton.medsolution.model.NewsArrayModel;

@Singleton
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "medsolution.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<NewsArrayModel, Long> newsArrayModelDao = null;

    @Inject
    public DatabaseHelper(Application application) {
        super(application, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHelper get(Context context) {
        return OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, NewsArrayModel.class);
            DataPersisterManager.registerDataPersisters(GsonStaticticsModelPersister.getSingleton());
            DataPersisterManager.registerDataPersisters(GsonNewsCategoryModelPersister.getSingleton());

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, NewsArrayModel.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Dao<NewsArrayModel, Long> getNewsArrayModelDao() throws SQLException {
        if (newsArrayModelDao == null)
            newsArrayModelDao = getDao(NewsArrayModel.class);
        return newsArrayModelDao;
    }

    public List<NewsArrayModel> getAllNewsArrayModel() {
        try {
            return getNewsArrayModelDao().queryForAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean createOrUpdateNewsArrayModel(NewsArrayModel model) {
        try {
            getNewsArrayModelDao().createOrUpdate(model);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean clearAllNewsArrayModel() {
        return dropTable(NewsArrayModel.class);
    }

    @Override
    public void close() {
        super.close();
        newsArrayModelDao = null;
    }

    public void clear() {
        clearAllNewsArrayModel();
    }

    public boolean dropTable(Class modelClass) {
        try {
            TableUtils.dropTable(connectionSource, modelClass, true);
            TableUtils.createTable(connectionSource, modelClass);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

}