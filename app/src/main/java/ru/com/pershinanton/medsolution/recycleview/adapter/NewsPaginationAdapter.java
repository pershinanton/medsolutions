package ru.com.pershinanton.medsolution.recycleview.adapter;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import ru.com.pershinanton.medsolution.R;
import ru.com.pershinanton.medsolution.model.NewsArrayModel;
import ru.com.pershinanton.medsolution.recycleview.adapter.utils.RequestLoadNextController;
import ru.com.pershinanton.medsolution.recycleview.adapter.viewholder.WallViewHolder;
import ru.com.pershinanton.medsolution.retrofit.ApiClient;
import ru.com.pershinanton.medsolution.utils.DateFormatter;
import ru.com.pershinanton.medsolution.utils.UilHelper;

public class NewsPaginationAdapter extends RecyclerView.Adapter<WallViewHolder> {

    private final List<NewsArrayModel> models = new ArrayList<>();

    private final RequestLoadNextController needLoadNextController = new RequestLoadNextController();

    private Listener listener;
    private LayoutInflater inflater;


    public NewsPaginationAdapter(Context context, Listener listener) {
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public WallViewHolder onCreateViewHolder(ViewGroup parent, int position) {

        return new WallViewHolder(inflater.inflate(R.layout.docs_wall_view, parent, false));
    }


    @Override
    public void onBindViewHolder(WallViewHolder viewHolder, final int position) {
        needLoadNextController.checkIsNeedLoadNext(getItemCount(), position);

        viewHolder.title.setText(String.valueOf(models.get(position).getTitle()));
        viewHolder.title.setTypeface(null, Typeface.BOLD);
        viewHolder.category.setText(models.get(position).getNewsCategoryModel().getTitle().toUpperCase());
        viewHolder.category.setTypeface(null, Typeface.BOLD);

        viewHolder.date.setText(DateFormatter.formatDateTime(models.get(position).getDate(), true));
        ImageLoader.getInstance().displayImage(models.get(position).getImageUrl(), viewHolder.url, UilHelper.getOptions()
                .resetViewBeforeLoading(true)
                .build());
        viewHolder.itemView.setOnClickListener(v -> {
            if (listener != null)
                listener.onClickWall(models.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void setModels(List<NewsArrayModel> models, boolean needLoadNextControllerEnabled) {
        this.models.clear();
        if (models != null)
            this.models.addAll(models);
        needLoadNextController.resetNeedLoadNextCounter();
        needLoadNextController.setEnabled(needLoadNextControllerEnabled);
        notifyDataSetChanged();
    }

    public void addModels(List<NewsArrayModel> models) {
        int oldSize = this.models.size();
        if (models != null)
            this.models.addAll(models);
        notifyItemRangeInserted(oldSize, models.size());
    }

    public void clearModels() {
        models.clear();
        notifyDataSetChanged();
    }

    public void resetNeedLoadNextCounter() {
        needLoadNextController.resetNeedLoadNextCounter();
    }

    public NewsPaginationAdapter setRequestLoadNextListener(RequestLoadNextController.OnRequestLoadNextListener onRequestLoadNextListener) {
        needLoadNextController.setRequestLoadNextListener(ApiClient.ITEMS_PER_PAGE / 2, onRequestLoadNextListener);
        return this;
    }

    public interface Listener {
        void onClickWall(NewsArrayModel VkWallModel);
    }

}
