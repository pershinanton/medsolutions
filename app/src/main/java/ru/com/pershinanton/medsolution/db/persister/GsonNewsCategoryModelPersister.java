package ru.com.pershinanton.medsolution.db.persister;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.StringType;

import javax.inject.Inject;

import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.model.NewsCategoryModel;

public class GsonNewsCategoryModelPersister extends StringType {

    private static final GsonNewsCategoryModelPersister INSTANCE = new GsonNewsCategoryModelPersister();
    @Inject
    Gson gson;

    public GsonNewsCategoryModelPersister() {
        super(SqlType.STRING, new Class<?>[]{new TypeToken<NewsCategoryModel>() {
        }.getClass()});
        App.INSTANCE.getAppComponent().inject(this);
    }

    public static GsonNewsCategoryModelPersister getSingleton() {
        return INSTANCE;
    }

    @Override
    public String javaToSqlArg(FieldType fieldType, Object NewsCategoryModel) {
        if (NewsCategoryModel != null)
            return gson.toJson(NewsCategoryModel, new TypeToken<NewsCategoryModel>() {
            }.getType());
        return null;
    }

    @Override
    public NewsCategoryModel sqlArgToJava(FieldType fieldType, Object sqlArg, int columnPos) {
        NewsCategoryModel NewsCategoryModel = null;
        if (sqlArg != null)
            NewsCategoryModel = gson.fromJson((String) sqlArg, new TypeToken<NewsCategoryModel>() {
            }.getType());
        return NewsCategoryModel;
    }

}