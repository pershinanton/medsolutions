package ru.com.pershinanton.medsolution.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.medsolution.App;
import ru.com.pershinanton.medsolution.R;
import ru.com.pershinanton.medsolution.Screens;
import ru.com.pershinanton.medsolution.main.FragmentLoginMvpPresenter;
import ru.com.pershinanton.medsolution.main.FragmentLoginMvpView;
import ru.com.pershinanton.medsolution.preferences.AuthPreferences;
import ru.com.pershinanton.medsolution.retrofit.model.LoginResponse;
import ru.com.pershinanton.medsolution.ui.base.BaseFragment;
import ru.terrakok.cicerone.Router;

public class FragmentLogin extends BaseFragment implements FragmentLoginMvpView {
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    @Inject
    Router router;
    @Inject
    AuthPreferences authPreferences;
    @InjectPresenter
    FragmentLoginMvpPresenter presenter;

    public FragmentLogin() {
        super(true, false, Orientation.NONE);
    }

    public static FragmentLogin getNewInstance() {
        FragmentLogin fragment = new FragmentLogin();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @ProvidePresenter
    public FragmentLoginMvpPresenter createPresenter() {
        return new FragmentLoginMvpPresenter(router);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.login(email.getText().toString(), password.getText().toString());
            }
        });
    }

    @Override
    public int onInflateLayout() {
        return R.layout.fragment_login;
    }

    @Override
    public void onDataLoaded(LoginResponse response) {
        authPreferences.setAccessToken(response.getAccessToken());
        authPreferences.setTokenType(response.getTokenType());
        router.navigateTo(Screens.FRAGMENT_WALL, null);
    }


    @Override
    public void onDataError(String message) {
        if (message != null)
            router.showSystemMessage(message);
    }
}
