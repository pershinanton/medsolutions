package ru.com.pershinanton.medsolution.retrofit.exception;

import java.io.IOException;

public class UnauthorizedIOException extends IOException {
    public UnauthorizedIOException() {
        super();
    }
}
