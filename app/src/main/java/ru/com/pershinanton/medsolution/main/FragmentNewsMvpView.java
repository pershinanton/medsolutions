package ru.com.pershinanton.medsolution.main;

import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import ru.com.pershinanton.medsolution.main.base.BaseNetworkMvpView;
import ru.com.pershinanton.medsolution.model.NewsArrayModel;

public interface FragmentNewsMvpView extends BaseNetworkMvpView {
    @StateStrategyType(AddToEndStrategy.class)
    void onDataLoaded(List<NewsArrayModel> items, boolean fromCache, int offset);
    @StateStrategyType(AddToEndStrategy.class)
    void onLoading(boolean isLoading);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onDataError(String message);


}
