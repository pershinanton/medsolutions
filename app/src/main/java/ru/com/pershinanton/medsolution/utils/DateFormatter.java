package ru.com.pershinanton.medsolution.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateFormatter {

    public static String formatDate(String dateStr, boolean useYear) {
        Date date = parseDate(dateStr);
        return formatStrDateTimeFree(date.getTime(), useYear);
    }

    public static String formatDateTime(String dateStr, boolean useYear) {
        Date date = parseDate(dateStr);
        return formatStrDateTimeFree(date.getTime(), useYear) + ", " + formatTime(date.getTime());
    }

    public static Date parseDate(String dateString) {
        if (dateString == null) return null;
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");
        try {
            return fmt.parse(dateString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatDate(Date date) {
        if (date == null) return null;
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm.SSS:ssz");
        try {
            return fmt.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formatStrDateTimeFree(long timeInMs, boolean useYear) {
        java.util.Date date = new java.util.Date(timeInMs);

        String dateStr = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int year = calendar.get(Calendar.YEAR);
        String monthStr = "";
        switch (month) {
            case Calendar.DECEMBER:
                monthStr = "дек.";
                break;
            case Calendar.JANUARY:
                monthStr = "янв.";
                break;
            case Calendar.FEBRUARY:
                monthStr = "фев.";
                break;
            case Calendar.MARCH:
                monthStr = "мар.";
                break;
            case Calendar.APRIL:
                monthStr = "апр.";
                break;
            case Calendar.MAY:
                monthStr = "мая";
                break;
            case Calendar.JUNE:
                monthStr = "Июн.";
                break;
            case Calendar.JULY:
                monthStr = "июл.";
                break;
            case Calendar.AUGUST:
                monthStr = "авг.";
                break;
            case Calendar.SEPTEMBER:
                monthStr = "сен.";
                break;
            case Calendar.OCTOBER:
                monthStr = "окт.";
                break;
            case Calendar.NOVEMBER:
                monthStr = "ноя.";
                break;
        }

        if (useYear)
            dateStr += day + " " + monthStr + " " + year;
        else
            dateStr += day + " " + monthStr;
        return dateStr.toLowerCase();
//        return showYeay ? date.replace("#", monthStr).toLowerCase() + " " + year : date.replace("#", monthStr).toLowerCase();
    }

    public static String formatTime(long timeInMs) {
        java.util.Date date = new java.util.Date(timeInMs);
        SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
        return localDateFormat.format(date);
    }

}
