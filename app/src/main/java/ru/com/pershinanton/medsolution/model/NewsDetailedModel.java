package ru.com.pershinanton.medsolution.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class NewsDetailedModel implements Parcelable {
    public static final Creator<NewsDetailedModel> CREATOR = new Creator<NewsDetailedModel>() {
        @Override
        public NewsDetailedModel createFromParcel(Parcel source) {
            return new NewsDetailedModel(source);
        }

        @Override
        public NewsDetailedModel[] newArray(int size) {
            return new NewsDetailedModel[size];
        }
    };
    @SerializedName("id")
    private int id;
    @SerializedName("link")
    private String url;
    @SerializedName("title")
    private String title;
    @SerializedName("statistics")
    private StaticticsModel staticticsModel;
    @SerializedName("created_at")
    private String date;
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("text")
    private String text;
    @SerializedName("news_category")
    private NewsCategoryModel newsCategoryModel;

    public NewsDetailedModel() {
    }

    protected NewsDetailedModel(Parcel in) {
        this.id = in.readInt();

    }

    public int getIds() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public StaticticsModel getStaticticsModel() {
        return staticticsModel;
    }

    public String getDate() {
        return date;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public NewsCategoryModel getNewsCategoryModel() {
        return newsCategoryModel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);


    }
}
