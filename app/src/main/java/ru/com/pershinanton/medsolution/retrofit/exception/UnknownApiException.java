package ru.com.pershinanton.medsolution.retrofit.exception;

public class UnknownApiException extends Exception {

    public UnknownApiException() {
        super();
    }

    public UnknownApiException(String message) {
        super(message);
    }

}
