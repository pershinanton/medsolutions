package ru.com.pershinanton.medsolution.main;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.com.pershinanton.medsolution.retrofit.model.LoginResponse;

public interface FragmentLoginMvpView extends MvpView {
    @StateStrategyType(AddToEndStrategy.class)
    void onDataLoaded(LoginResponse response);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onDataError(String message);


}
