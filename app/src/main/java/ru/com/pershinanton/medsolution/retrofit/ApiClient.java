package ru.com.pershinanton.medsolution.retrofit;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.com.pershinanton.medsolution.retrofit.model.LoginResponse;
import ru.com.pershinanton.medsolution.retrofit.model.NewsResponse;
import ru.com.pershinanton.medsolution.retrofit.model.WallResponse;


public interface ApiClient {

    String BASE_HOST = "medicapp.mhth.ru/";
    String BASE_URL = "https://" + BASE_HOST;
    String PAGE = "page";
    String PER = "per";
    String GRANT_TYPE = "grant_type";
    String USER_NAME = "username";
    String PASSWORD = "password";

    int ITEMS_PER_PAGE = 10;


    @POST("oauth/token")
    Call<LoginResponse> loginPost(@Header("Client-Type") String clientType, @Header("Accept") String accept, @Query(GRANT_TYPE) String grantType, @Query(USER_NAME) String userName, @Query(PASSWORD) String password);

    //https://medicapp.mhth.ru/oauth/token
    @GET("api/v1/news")
    Call<WallResponse> wallGet(@Query(PAGE) int page, @Query(PER) int per);

    //https://medicapp.mhth.ru/api/v1/news?page=2&per=1
    @GET("api/v1/news/{pageId}")
    Call<NewsResponse> newsGet(@Path("pageId") long pageId);


}
