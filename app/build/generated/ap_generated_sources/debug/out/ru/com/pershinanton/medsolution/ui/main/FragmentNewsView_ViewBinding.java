// Generated code from Butter Knife. Do not modify!
package ru.com.pershinanton.medsolution.ui.main;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pnikosis.materialishprogress.ProgressWheel;
import java.lang.IllegalStateException;
import java.lang.Override;
import org.sufficientlysecure.htmltextview.HtmlTextView;
import ru.com.pershinanton.medsolution.R;

public class FragmentNewsView_ViewBinding implements Unbinder {
  private FragmentNewsView target;

  @UiThread
  public FragmentNewsView_ViewBinding(FragmentNewsView target, View source) {
    this.target = target;

    target.collapsing_toolbar_layout = Utils.findRequiredViewAsType(source, R.id.collapsing_toolbar_layout, "field 'collapsing_toolbar_layout'", CollapsingToolbarLayout.class);
    target.app_bar_layout = Utils.findRequiredViewAsType(source, R.id.app_bar_layout, "field 'app_bar_layout'", AppBarLayout.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    target.header_image_view = Utils.findRequiredViewAsType(source, R.id.header_image_view, "field 'header_image_view'", ImageView.class);
    target.category = Utils.findRequiredViewAsType(source, R.id.category, "field 'category'", HtmlTextView.class);
    target.textView = Utils.findRequiredViewAsType(source, R.id.textView, "field 'textView'", HtmlTextView.class);
    target.date = Utils.findRequiredViewAsType(source, R.id.date, "field 'date'", TextView.class);
    target.title = Utils.findRequiredViewAsType(source, R.id.title, "field 'title'", HtmlTextView.class);
    target.buttonLink = Utils.findRequiredViewAsType(source, R.id.buttonLink, "field 'buttonLink'", TextView.class);
    target.progressWheel = Utils.findRequiredViewAsType(source, R.id.progress_wheel, "field 'progressWheel'", ProgressWheel.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FragmentNewsView target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.collapsing_toolbar_layout = null;
    target.app_bar_layout = null;
    target.toolbar = null;
    target.header_image_view = null;
    target.category = null;
    target.textView = null;
    target.date = null;
    target.title = null;
    target.buttonLink = null;
    target.progressWheel = null;
  }
}
