package ru.com.pershinanton.medsolution.main;

import java.util.Set;

import com.arellomobile.mvp.viewstate.MvpViewState;
import com.arellomobile.mvp.viewstate.ViewCommand;
import com.arellomobile.mvp.viewstate.ViewCommands;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategy;

public class FragmentNewsDetailedMvpView$$State extends MvpViewState<ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView> implements ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView {
	private ViewCommands<ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView> mViewCommands = new ViewCommands<>();

	@Override
	public void restoreState(ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView view, Set<ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView>> currentState) {
		if (mViewCommands.isEmpty()) {
			return;
		}

		mViewCommands.reapply(view, currentState);
	}

	@Override
	public  void onDataLoaded(ru.com.pershinanton.medsolution.model.NewsDetailedModel newsDetailedModel) {
		OnDataLoadedCommand onDataLoadedCommand = new OnDataLoadedCommand(newsDetailedModel);
		mViewCommands.beforeApply(onDataLoadedCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView view : mViews) {
			getCurrentState(view).add(onDataLoadedCommand);
			view.onDataLoaded(newsDetailedModel);
		}

		mViewCommands.afterApply(onDataLoadedCommand);
	}

	@Override
	public  void onLoading(boolean isLoading) {
		OnLoadingCommand onLoadingCommand = new OnLoadingCommand(isLoading);
		mViewCommands.beforeApply(onLoadingCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView view : mViews) {
			getCurrentState(view).add(onLoadingCommand);
			view.onLoading(isLoading);
		}

		mViewCommands.afterApply(onLoadingCommand);
	}

	@Override
	public  void onDataError(java.lang.String message) {
		OnDataErrorCommand onDataErrorCommand = new OnDataErrorCommand(message);
		mViewCommands.beforeApply(onDataErrorCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView view : mViews) {
			getCurrentState(view).add(onDataErrorCommand);
			view.onDataError(message);
		}

		mViewCommands.afterApply(onDataErrorCommand);
	}


	public class OnDataLoadedCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView> {
		public final ru.com.pershinanton.medsolution.model.NewsDetailedModel newsDetailedModel;

		OnDataLoadedCommand(ru.com.pershinanton.medsolution.model.NewsDetailedModel newsDetailedModel) {
			super("onDataLoaded", com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy.class);
			this.newsDetailedModel = newsDetailedModel;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView mvpView) {
			mvpView.onDataLoaded(newsDetailedModel);
			getCurrentState(mvpView).add(this);
		}
	}

	public class OnLoadingCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView> {
		public final boolean isLoading;

		OnLoadingCommand(boolean isLoading) {
			super("onLoading", com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy.class);
			this.isLoading = isLoading;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView mvpView) {
			mvpView.onLoading(isLoading);
			getCurrentState(mvpView).add(this);
		}
	}

	public class OnDataErrorCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView> {
		public final java.lang.String message;

		OnDataErrorCommand(java.lang.String message) {
			super("onDataError", com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy.class);
			this.message = message;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView mvpView) {
			mvpView.onDataError(message);
			getCurrentState(mvpView).add(this);
		}
	}
}
