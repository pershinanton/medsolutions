package ru.com.pershinanton.medsolution.main;

import com.arellomobile.mvp.ViewStateProvider;
import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.MvpViewState;

public class FragmentNewsDetailedMvpPresenter$$ViewStateProvider extends ViewStateProvider {
	
	@Override
	public MvpViewState<? extends MvpView> getViewState() {
		return new ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpView$$State();
	}
}