package ru.com.pershinanton.medsolution.main;

import java.util.Set;

import com.arellomobile.mvp.viewstate.MvpViewState;
import com.arellomobile.mvp.viewstate.ViewCommand;
import com.arellomobile.mvp.viewstate.ViewCommands;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategy;

public class FragmentNewsMvpView$$State extends MvpViewState<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView> implements ru.com.pershinanton.medsolution.main.FragmentNewsMvpView {
	private ViewCommands<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView> mViewCommands = new ViewCommands<>();

	@Override
	public void restoreState(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView view, Set<ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView>> currentState) {
		if (mViewCommands.isEmpty()) {
			return;
		}

		mViewCommands.reapply(view, currentState);
	}

	@Override
	public  void onDataLoaded(java.util.List<ru.com.pershinanton.medsolution.model.NewsArrayModel> items, boolean fromCache, int offset) {
		OnDataLoadedCommand onDataLoadedCommand = new OnDataLoadedCommand(items, fromCache, offset);
		mViewCommands.beforeApply(onDataLoadedCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView view : mViews) {
			getCurrentState(view).add(onDataLoadedCommand);
			view.onDataLoaded(items, fromCache, offset);
		}

		mViewCommands.afterApply(onDataLoadedCommand);
	}

	@Override
	public  void onLoading(boolean isLoading) {
		OnLoadingCommand onLoadingCommand = new OnLoadingCommand(isLoading);
		mViewCommands.beforeApply(onLoadingCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView view : mViews) {
			getCurrentState(view).add(onLoadingCommand);
			view.onLoading(isLoading);
		}

		mViewCommands.afterApply(onLoadingCommand);
	}

	@Override
	public  void onDataError(java.lang.String message) {
		OnDataErrorCommand onDataErrorCommand = new OnDataErrorCommand(message);
		mViewCommands.beforeApply(onDataErrorCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView view : mViews) {
			getCurrentState(view).add(onDataErrorCommand);
			view.onDataError(message);
		}

		mViewCommands.afterApply(onDataErrorCommand);
	}

	@Override
	public  void unknownException() {
		UnknownExceptionCommand unknownExceptionCommand = new UnknownExceptionCommand();
		mViewCommands.beforeApply(unknownExceptionCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView view : mViews) {
			getCurrentState(view).add(unknownExceptionCommand);
			view.unknownException();
		}

		mViewCommands.afterApply(unknownExceptionCommand);
	}

	@Override
	public  void apiException(java.lang.String message) {
		ApiExceptionCommand apiExceptionCommand = new ApiExceptionCommand(message);
		mViewCommands.beforeApply(apiExceptionCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView view : mViews) {
			getCurrentState(view).add(apiExceptionCommand);
			view.apiException(message);
		}

		mViewCommands.afterApply(apiExceptionCommand);
	}

	@Override
	public  void connectionException() {
		ConnectionExceptionCommand connectionExceptionCommand = new ConnectionExceptionCommand();
		mViewCommands.beforeApply(connectionExceptionCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView view : mViews) {
			getCurrentState(view).add(connectionExceptionCommand);
			view.connectionException();
		}

		mViewCommands.afterApply(connectionExceptionCommand);
	}


	public class OnDataLoadedCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView> {
		public final java.util.List<ru.com.pershinanton.medsolution.model.NewsArrayModel> items;
		public final boolean fromCache;
		public final int offset;

		OnDataLoadedCommand(java.util.List<ru.com.pershinanton.medsolution.model.NewsArrayModel> items, boolean fromCache, int offset) {
			super("onDataLoaded", com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy.class);
			this.items = items;
			this.fromCache = fromCache;
			this.offset = offset;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView mvpView) {
			mvpView.onDataLoaded(items, fromCache, offset);
			getCurrentState(mvpView).add(this);
		}
	}

	public class OnLoadingCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView> {
		public final boolean isLoading;

		OnLoadingCommand(boolean isLoading) {
			super("onLoading", com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy.class);
			this.isLoading = isLoading;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView mvpView) {
			mvpView.onLoading(isLoading);
			getCurrentState(mvpView).add(this);
		}
	}

	public class OnDataErrorCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView> {
		public final java.lang.String message;

		OnDataErrorCommand(java.lang.String message) {
			super("onDataError", com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy.class);
			this.message = message;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView mvpView) {
			mvpView.onDataError(message);
			getCurrentState(mvpView).add(this);
		}
	}

	public class UnknownExceptionCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView> {
		UnknownExceptionCommand() {
			super("unknownException", com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy.class);
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView mvpView) {
			mvpView.unknownException();
			getCurrentState(mvpView).add(this);
		}
	}

	public class ApiExceptionCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView> {
		public final java.lang.String message;

		ApiExceptionCommand(java.lang.String message) {
			super("apiException", com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy.class);
			this.message = message;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView mvpView) {
			mvpView.apiException(message);
			getCurrentState(mvpView).add(this);
		}
	}

	public class ConnectionExceptionCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentNewsMvpView> {
		ConnectionExceptionCommand() {
			super("connectionException", com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy.class);
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentNewsMvpView mvpView) {
			mvpView.connectionException();
			getCurrentState(mvpView).add(this);
		}
	}
}
