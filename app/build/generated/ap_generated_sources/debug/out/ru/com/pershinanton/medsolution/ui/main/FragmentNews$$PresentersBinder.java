package ru.com.pershinanton.medsolution.ui.main;

import java.util.ArrayList;
import java.util.List;

import com.arellomobile.mvp.PresenterBinder;
import com.arellomobile.mvp.presenter.PresenterField;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.presenter.PresenterType;

public class FragmentNews$$PresentersBinder extends PresenterBinder<ru.com.pershinanton.medsolution.ui.main.FragmentNews> {
	public class presenterBinder extends PresenterField {
		public presenterBinder() {
			super(null, PresenterType.LOCAL, null, ru.com.pershinanton.medsolution.main.FragmentNewsMvpPresenter.class);
		}

		@Override
		public void bind(Object target, MvpPresenter presenter) {
			((ru.com.pershinanton.medsolution.ui.main.FragmentNews) target).presenter = (ru.com.pershinanton.medsolution.main.FragmentNewsMvpPresenter) presenter;
		}

		@Override
		public MvpPresenter<?> providePresenter(Object delegated) {
			return ((ru.com.pershinanton.medsolution.ui.main.FragmentNews) delegated).createPresenter();
		}
	}

	public List<PresenterField<?, ? super ru.com.pershinanton.medsolution.ui.main.FragmentNews>> getPresenterFields() {
		List<PresenterField<?, ? super ru.com.pershinanton.medsolution.ui.main.FragmentNews>> presenters = new ArrayList<>();

		presenters.add(new presenterBinder());

		return presenters;
	}

}
