package ru.com.pershinanton.medsolution.ui.main;

import java.util.ArrayList;
import java.util.List;

import com.arellomobile.mvp.PresenterBinder;
import com.arellomobile.mvp.presenter.PresenterField;
import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.presenter.PresenterType;

public class FragmentLogin$$PresentersBinder extends PresenterBinder<ru.com.pershinanton.medsolution.ui.main.FragmentLogin> {
	public class presenterBinder extends PresenterField {
		public presenterBinder() {
			super(null, PresenterType.LOCAL, null, ru.com.pershinanton.medsolution.main.FragmentLoginMvpPresenter.class);
		}

		@Override
		public void bind(Object target, MvpPresenter presenter) {
			((ru.com.pershinanton.medsolution.ui.main.FragmentLogin) target).presenter = (ru.com.pershinanton.medsolution.main.FragmentLoginMvpPresenter) presenter;
		}

		@Override
		public MvpPresenter<?> providePresenter(Object delegated) {
			return ((ru.com.pershinanton.medsolution.ui.main.FragmentLogin) delegated).createPresenter();
		}
	}

	public List<PresenterField<?, ? super ru.com.pershinanton.medsolution.ui.main.FragmentLogin>> getPresenterFields() {
		List<PresenterField<?, ? super ru.com.pershinanton.medsolution.ui.main.FragmentLogin>> presenters = new ArrayList<>();

		presenters.add(new presenterBinder());

		return presenters;
	}

}
