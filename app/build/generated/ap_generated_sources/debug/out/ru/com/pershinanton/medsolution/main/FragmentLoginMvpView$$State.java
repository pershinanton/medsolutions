package ru.com.pershinanton.medsolution.main;

import java.util.Set;

import com.arellomobile.mvp.viewstate.MvpViewState;
import com.arellomobile.mvp.viewstate.ViewCommand;
import com.arellomobile.mvp.viewstate.ViewCommands;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategy;

public class FragmentLoginMvpView$$State extends MvpViewState<ru.com.pershinanton.medsolution.main.FragmentLoginMvpView> implements ru.com.pershinanton.medsolution.main.FragmentLoginMvpView {
	private ViewCommands<ru.com.pershinanton.medsolution.main.FragmentLoginMvpView> mViewCommands = new ViewCommands<>();

	@Override
	public void restoreState(ru.com.pershinanton.medsolution.main.FragmentLoginMvpView view, Set<ViewCommand<ru.com.pershinanton.medsolution.main.FragmentLoginMvpView>> currentState) {
		if (mViewCommands.isEmpty()) {
			return;
		}

		mViewCommands.reapply(view, currentState);
	}

	@Override
	public  void onDataLoaded(ru.com.pershinanton.medsolution.retrofit.model.LoginResponse response) {
		OnDataLoadedCommand onDataLoadedCommand = new OnDataLoadedCommand(response);
		mViewCommands.beforeApply(onDataLoadedCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentLoginMvpView view : mViews) {
			getCurrentState(view).add(onDataLoadedCommand);
			view.onDataLoaded(response);
		}

		mViewCommands.afterApply(onDataLoadedCommand);
	}

	@Override
	public  void onDataError(java.lang.String message) {
		OnDataErrorCommand onDataErrorCommand = new OnDataErrorCommand(message);
		mViewCommands.beforeApply(onDataErrorCommand);

		if (mViews == null || mViews.isEmpty()) {
			return;
		}

		for(ru.com.pershinanton.medsolution.main.FragmentLoginMvpView view : mViews) {
			getCurrentState(view).add(onDataErrorCommand);
			view.onDataError(message);
		}

		mViewCommands.afterApply(onDataErrorCommand);
	}


	public class OnDataLoadedCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentLoginMvpView> {
		public final ru.com.pershinanton.medsolution.retrofit.model.LoginResponse response;

		OnDataLoadedCommand(ru.com.pershinanton.medsolution.retrofit.model.LoginResponse response) {
			super("onDataLoaded", com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy.class);
			this.response = response;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentLoginMvpView mvpView) {
			mvpView.onDataLoaded(response);
			getCurrentState(mvpView).add(this);
		}
	}

	public class OnDataErrorCommand extends ViewCommand<ru.com.pershinanton.medsolution.main.FragmentLoginMvpView> {
		public final java.lang.String message;

		OnDataErrorCommand(java.lang.String message) {
			super("onDataError", com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy.class);
			this.message = message;
		}

		@Override
		public void apply(ru.com.pershinanton.medsolution.main.FragmentLoginMvpView mvpView) {
			mvpView.onDataError(message);
			getCurrentState(mvpView).add(this);
		}
	}
}
