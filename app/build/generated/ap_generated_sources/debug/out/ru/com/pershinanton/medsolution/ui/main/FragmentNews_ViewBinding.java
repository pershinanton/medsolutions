// Generated code from Butter Knife. Do not modify!
package ru.com.pershinanton.medsolution.ui.main;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.pnikosis.materialishprogress.ProgressWheel;
import java.lang.IllegalStateException;
import java.lang.Override;
import ru.com.pershinanton.medsolution.R;

public class FragmentNews_ViewBinding implements Unbinder {
  private FragmentNews target;

  @UiThread
  public FragmentNews_ViewBinding(FragmentNews target, View source) {
    this.target = target;

    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar1, "field 'toolbar'", Toolbar.class);
    target.progressWheel = Utils.findRequiredViewAsType(source, R.id.progress_wheel, "field 'progressWheel'", ProgressWheel.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FragmentNews target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.toolbar = null;
    target.progressWheel = null;
  }
}
