// Generated code from Butter Knife. Do not modify!
package ru.com.pershinanton.medsolution.ui.main;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;
import ru.com.pershinanton.medsolution.R;

public class FragmentLogin_ViewBinding implements Unbinder {
  private FragmentLogin target;

  @UiThread
  public FragmentLogin_ViewBinding(FragmentLogin target, View source) {
    this.target = target;

    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", EditText.class);
    target.password = Utils.findRequiredViewAsType(source, R.id.password, "field 'password'", EditText.class);
    target.buttonLogin = Utils.findRequiredViewAsType(source, R.id.buttonLogin, "field 'buttonLogin'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FragmentLogin target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.email = null;
    target.password = null;
    target.buttonLogin = null;
  }
}
