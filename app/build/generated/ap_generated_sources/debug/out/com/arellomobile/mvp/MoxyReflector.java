package com.arellomobile.mvp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MoxyReflector {

	private static Map<Class<?>, Object> sViewStateProviders;
	private static Map<Class<?>, List<Object>> sPresenterBinders;
	private static Map<Class<?>, Object> sStrategies;

	static {
		sViewStateProviders = new HashMap<>();
		sViewStateProviders.put(ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpPresenter.class, new ru.com.pershinanton.medsolution.main.FragmentNewsDetailedMvpPresenter$$ViewStateProvider());
		sViewStateProviders.put(ru.com.pershinanton.medsolution.main.FragmentLoginMvpPresenter.class, new ru.com.pershinanton.medsolution.main.FragmentLoginMvpPresenter$$ViewStateProvider());
		sViewStateProviders.put(ru.com.pershinanton.medsolution.main.FragmentNewsMvpPresenter.class, new ru.com.pershinanton.medsolution.main.FragmentNewsMvpPresenter$$ViewStateProvider());
		
		sPresenterBinders = new HashMap<>();
		sPresenterBinders.put(ru.com.pershinanton.medsolution.ui.main.FragmentNews.class, Arrays.<Object>asList(new ru.com.pershinanton.medsolution.ui.main.FragmentNews$$PresentersBinder()));
		sPresenterBinders.put(ru.com.pershinanton.medsolution.ui.main.FragmentLogin.class, Arrays.<Object>asList(new ru.com.pershinanton.medsolution.ui.main.FragmentLogin$$PresentersBinder()));
		sPresenterBinders.put(ru.com.pershinanton.medsolution.ui.main.FragmentNewsView.class, Arrays.<Object>asList(new ru.com.pershinanton.medsolution.ui.main.FragmentNewsView$$PresentersBinder()));
		
		sStrategies = new HashMap<>();
		sStrategies.put(com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy.class, new com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy());
		sStrategies.put(com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy.class, new com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy());
		sStrategies.put(com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy.class, new com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy());
	}
	
	public static Object getViewState(Class<?> presenterClass) {
		ViewStateProvider viewStateProvider = (ViewStateProvider) sViewStateProviders.get(presenterClass);
		if (viewStateProvider == null) {
			return null;
		}
		
		return viewStateProvider.getViewState();
	}

	public static List<Object> getPresenterBinders(Class<?> delegated) {
		return sPresenterBinders.get(delegated);
	}
	public static Object getStrategy(Class<?> strategyClass) {
		return sStrategies.get(strategyClass);
	}
}
